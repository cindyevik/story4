from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('works/', views.works, name='works'),
    path('gallery/', views.gallery, name='gallery'),
    path('resume/', views.resume, name='resume'),
    path('sched_add/', views.sched_add, name='sched_add'),
    path('schedule/', views.sched_detail, name='sched_detail'),
    path('sched_edit/<int:pk>', views.sched_edit, name='sched_edit'),
    path('sched_delete/<int:pk>', views.sched_delete, name='sched_delete'),

]