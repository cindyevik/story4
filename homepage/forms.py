from django import forms
from .models import Schedule
from django.forms import ModelForm

class ScheduleForm(ModelForm):
    class Meta:
        model= Schedule
        fields = ['dates', 'time', 'activity', 'location', 'category']
        labels = {
            'dates' : 'Date', 'time' : 'Time', 'activity' : 'Activity', 'location' : 'Location', 'category' : 'Category'
        }
        widgets = {
            'dates' : forms.DateInput(attrs={'class': 'form-control',
                                        'type' : 'date',
                                        'placeholder' : 'yyyy-mm-dd'}),
            'time' : forms.TimeInput(attrs={'class': 'form-control',
                                        'type' : 'time',
                                        'placeholder' : 'hh:mm:ss'}),
            'activity' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'what are you going to do??'}),
            'location' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'e.g : Kampus'}),
            'category' : forms.TextInput(attrs={'class': 'form-control',
                                                'placeholder' : 'e.g : Exam'})
        }
