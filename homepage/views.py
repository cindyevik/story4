from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
from django.shortcuts import redirect
import datetime


# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'About.html')

def works(request):
    return render(request, 'works.html')

def gallery(request):
    return render(request, 'gallery.html')

def resume(request):
    return render(request, 'portofolio.html')

def form(request):
    content = {'title' : 'Form Registration'}
    return render(request, 'Forms.html', content)
def sched_add(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('sched_detail')
    else:
        form = ScheduleForm()

    content = {'title' : 'Form Schedule',
                'form' : form}

    return render(request, 'Schedule_Form.html', content)

def sched_detail(request):
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return render(request, 'Schedule.html', content)

def sched_edit(request, pk):
    post = Schedule.objects.get(pk=pk)
    if request.method == "POST":
        form = ScheduleForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('sched_detail')
    else:
        form = ScheduleForm(instance=post)

    content = {'title' : 'Form Schedule',
                'form' : form,
                'obj' : post}
    return render(request, 'Schedule_Edit.html', content)

def sched_delete(request, pk):
    Schedule.objects.filter(pk=pk).delete()
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return redirect('sched_detail')
